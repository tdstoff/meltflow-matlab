%- File: 'prm_call.m'
%- Purpose: Call parameters from vector to main file
[n_dim,n_var,n,dx,x_min,x_max,t_f,cfl,flg_fld,EoS,c_EoS,slvr,flg_BCs,n_nds,ICs_hdr,n_disp,n_out,flg_intrp,flg_wrt,wrt_nm,flg_plt,opt_plt,n_r,e_r,flg_anmt,n_anmt,t_anmt,wrt_prfx,wrt_sfx,n_rstrt,rd_nm,rd_prfx,rd_sfx,plt_ps,plt_wn,flg_vec,n_vec,t_0] = deal(prm{1:38});