function files
%- Purpose: Add file paths to main script
addpath('functions/');                  % Location of functions
addpath('functions/geometry/');         % Location of geometry functions
addpath('input/');                      % Location of input files